<?php

/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 */

/**
 * Implements hook_preprocess_page().
 */
function wwuzen_alumni_foundation_preprocess_page(&$variables) {
  drupal_add_js('https://www.wwu.edu/wwucommon/js/lib/wwu/wwu-dropdown.min.js', 'external');

  $variables['logo_is_field'] = FALSE;
  $variables['disable_header'] = FALSE;

  if (isset($variables['node'])) {
    $node = $variables['node'];

    $disable_header = field_get_items('node', $node, 'field_disable_header', $node->language);

    if ($disable_header && $disable_header[0]['value'] === '1') {
      $variables['disable_header'] = TRUE;
    }

    // Add a page template hook suggestion for each node type.
    $variables['theme_hook_suggestions'][] = 'page__' . $node->type;

    $logo = field_view_field('node', $node, 'field_header', array(
      'label' => 'hidden',
      'type' => 'file_rendered',
      'settings' => array(
        'file_view_mode' => 'header',
      ),
    ));

    if ($logo) {
      $variables['logo'] = $logo;
      $variables['logo_is_field'] = TRUE;
    }
  }

  $variables['address'] = theme_get_setting('address');
  $variables['phone'] = theme_get_setting('phone');
  $variables['email'] = theme_get_setting('email');
}

/**
 * Implements hook_preprocess_node().
 */
function wwuzen_alumni_foundation_preprocess_node(&$variables) {
  $node = $variables['node'];
  $type = $variables['node']->type;

  if ($variables['teaser']) {
    $variables['theme_hook_suggestions'][] = "node__{$type}__teaser";
  }

  $title_decoded = html_entity_decode($variables['title'], ENT_QUOTES);
  $variables['classes_array'][] = drupal_html_class("node-{$type}-{$title_decoded}");
}

/**
 * Implements hook_preprocess_panels_pane().
 */
function wwuzen_alumni_foundation_preprocess_panels_pane(&$variables) {
  $pane = $variables['pane'];

  if (!empty($pane->configuration)) {
    if (!empty($pane->configuration['admin_title'])) {
      $admin_title = $pane->configuration['admin_title'];
      $variables['classes_array'][] = 'pane-admin-title-' . drupal_html_class($admin_title);
    }

    if (!empty($pane->configuration['title'])) {
      $title = $pane->configuration['title'];
      $variables['classes_array'][] = 'pane-title-' . drupal_html_class($title);
    }
  }
}

/**
 * Implements hook_preprocess_html().
 */
function wwuzen_alumni_foundation_preprocess_html(&$variables) {
  // Add class to body if certain node type.
  if ($node = menu_get_object('node')) {
    $disable_header = field_get_items('node', $node, 'field_disable_header', $node->language);

    if ($node->type === 'event' || ($disable_header && $disable_header[0]['value'] === '1')) {
      $variables['classes_array'][] = 'no-logo';
    }
  }
}

/**
 * Implements hook_preprocess_date_display_range().
 */
function wwuzen_alumni_foundation_preprocess_date_display_range(&$variables) {
  $date1 = $variables['dates']['value']['local']['object'];
  $date2 = $variables['dates']['value2']['local']['object'];

  if ((int) $date1->format('n') == (int) $date2->format('n')) {
    $variables['date2'] = $date2->format('j');
  }
}

/**
 * Implements theme_date_display_range().
 */
function wwuzen_alumni_foundation_date_display_range(&$variables) {
  $date1 = $variables['date1'];
  $date2 = $variables['date2'];
  $timezone = $variables['timezone'];
  $attributes_start = $variables['attributes_start'];
  $attributes_end = $variables['attributes_end'];

  $start_date = '<span class="date-display-start"' . drupal_attributes($attributes_start) . '>' . $date1 . '</span>';
  $end_date = '<span class="date-display-end"' . drupal_attributes($attributes_end) . '>' . $date2 . $timezone . '</span>';

  if (!empty($variables['add_microdata'])) {
    $start_date .= '<meta' . drupal_attributes($variables['microdata']['value']['#attributes']) . '/>';
    $end_date .= '<meta' . drupal_attributes($variables['microdata']['value2']['#attributes']) . '/>';
  }

  $output = '<span class="date-display-range">' . t('!start-date &ndash; !end-date', array(
    '!start-date' => $start_date,
    '!end-date' => $end_date,
  )) . '</span>';

  return $output;
}
