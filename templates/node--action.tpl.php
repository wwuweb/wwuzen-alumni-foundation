<?php
/**
 * @file
 * Node Action template.
 */
?>
<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print render($content['field_photo']); ?>

  <section class="action-content" >
    <?php if ($title_prefix || $title_suffix || $display_submitted || $unpublished || !$page && $title): ?>
    <header>
      <?php print render($title_prefix); ?>

      <?php if (!$page && $title): ?>
      <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
      <?php endif; ?>

      <?php print render($title_suffix); ?>

      <?php if ($unpublished): ?>
      <mark class="unpublished"><?php print t('Unpublished'); ?></mark>
      <?php endif; ?>
    </header>
    <?php endif; ?>

    <?php
      print render($content['body']);
      print render($content['field_link']);
    ?>
  </section>

</article>
