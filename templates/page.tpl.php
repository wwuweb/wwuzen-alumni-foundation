<?php

/**
* @file
* Zen theme's implementation to display a single Drupal page.
*
* Available variables:
*
* General utility variables:
* - $base_path: The base URL path of the Drupal installation. At the very
*   least, this will always default to /.
* - $directory: The directory the template is located in, e.g. modules/system
*   or themes/bartik.
* - $is_front: TRUE if the current page is the front page.
* - $logged_in: TRUE if the user is registered and signed in.
* - $is_admin: TRUE if the user has permission to access administration pages.
*
* Site identity:
* - $front_page: The URL of the front page. Use this instead of $base_path,
*   when linking to the front page. This includes the language domain or
*   prefix.
* - $logo: The path to the logo image, as defined in theme configuration.
* - $site_name: The name of the site, empty when display has been disabled
*   in theme settings.
* - $site_slogan: The slogan of the site, empty when display has been disabled
*   in theme settings.
*
* Navigation:
* - $main_menu (array): An array containing the Main menu links for the
*   site, if they have been configured.
* - $secondary_menu (array): An array containing the Secondary menu links for
*   the site, if they have been configured.
* - $secondary_menu_heading: The title of the menu used by the secondary links.
* - $breadcrumb: The breadcrumb trail for the current page.
*
* Page content (in order of occurrence in the default page.tpl.php):
* - $title_prefix (array): An array containing additional output populated by
*   modules, intended to be displayed in front of the main title tag that
*   appears in the template.
* - $title: The page title, for use in the actual HTML content.
* - $title_suffix (array): An array containing additional output populated by
*   modules, intended to be displayed after the main title tag that appears in
*   the template.
* - $messages: HTML for status and error messages. Should be displayed
*   prominently.
* - $tabs (array): Tabs linking to any sub-pages beneath the current page
*   (e.g., the view and edit tabs when displaying a node).
* - $action_links (array): Actions local to the page, such as 'Add menu' on the
*   menu administration interface.
* - $feed_icons: A string of all feed icons for the current page.
* - $node: The node object, if there is an automatically-loaded node
*   associated with the page, and the node ID is the second argument
*   in the page's path (e.g. node/12345 and node/12345/revisions, but not
*   comment/reply/12345).
*
* Regions:
* - $page['header']: Items for the header region.
* - $page['navigation']: Items for the navigation region, below the main menu
*   (if any).
* - $page['help']: Dynamic help text, mostly for admin pages.
* - $page['highlighted']: Items for the highlighted content region.
* - $page['content']: The main content of the current page.
* - $page['sidebar_first']: Items for the first sidebar.
* - $page['sidebar_second']: Items for the second sidebar.
* - $page['footer']: Items for the footer region.
* - $page['bottom']: Items to appear at the bottom of the page below the
*   footer.
*
* @see template_preprocess()
* @see template_preprocess_page()
* @see zen_preprocess_page()
* @see template_process()
*/
?>

<div class="page">

  <header class="header-wrap">
    <!-- START WESTERN HEADER -->
    <section class="western-header" aria-label="University Links, Search, and Navigation">
      <div class="center-content">
        <nav aria-label="Quick links">
          <div class="western-quick-links">
                   <button>Toggle Quick Links</button>
            <ul>
              <li><a href="http://forms.alumni.wwu.edu/s/1710/forms/interior-alumni.aspx?sid=1710&gid=2&pgid=8&cid=46" title="Register"><span aria-hidden="true">c</span> <span>Register</span></a></li>
              <li><a href="https://securelb.imodules.com/s/1710/forms/interior-alumni.aspx?sid=1710&gid=2&pgid=3&cid=40&returnUrl=http%3a%2f%2fforms.alumni.wwu.edu%2fs%2f1710%2fforms%2finterior-alumni.aspx%3fsid%3d1710%26gid%3d2%26pgid%3d578" title="Login"><span aria-hidden="true">i</span> <span>Login</span></a></li>
              <li><a href="https://securelb.imodules.com/s/1710/forms/interior-alumni.aspx?sid=1710&gid=2&pgid=682&cid=1559" title="Update Info"><span aria-hidden="true">d</span> <span>Update Info</span></a></li>
              <li><a href="http://www.wwu.edu/index" title="Index"><span aria-hidden="true">i</span> <span>Index</span></a></li>
              <li><a href="http://www.wwu.edu/campusmaps" title="Map"><span aria-hidden="true">l</span> <span>Map</span></a></li>
            </ul>
          </div>
          <div class="western-search" role="search" aria-label="University Site">
            <button>Open Search</button>
            <div class="western-search-widget">
              <?php print $search_box; ?>
            </div>
          </div>
          <button class="mobile-main-nav">Open Main Navigation</button>
        </nav>
      </div>
      <span class="western-logo">
        <a href="<?php print $front_page; ?>">Western Washington University Alumni</a>
      </span>
    </section>
    <!-- END WESTERN HEADER -->

    <div class="main-menu-wrap">
      <nav class="main-nav" id="main-menu" aria-label="Main">
        <?php print render($page['navigation']); ?>
      </nav>
      <?php print render($page['header']); ?>
    </div>
  </header>

  <section class="site-header" aria-label="Site Header">
    <?php if (!$disable_header): ?>
      <?php if ($logo_is_field): ?>
        <div class="site-banner">
          <?php print render($logo); ?>
        </div>
      <?php elseif ($logo): ?>
        <div class="site-banner">
          <img src="<?php print $logo;?>" alt="">
        </div>
      <?php endif; ?>
    <?php endif; ?>

    <?php if ($site_name): ?>
      <div class="site-name">
        <p><span><?php print $site_name; ?></span></p>
      </div>
    <?php endif; ?>
  </section>

  <main>
    <div class="center-content">

      <?php if ($title): ?>
        <header class="page-title">
          <?php print render($title_prefix); ?>
          <h1><?php print $title; ?></h1>
          <?php print $breadcrumb; ?>
        </header>
      <?php endif; ?>

      <div class="content column">
        <?php print render($page['highlighted']); ?>
        <?php print render($title_suffix); ?>
        <?php print $messages; ?>
        <?php print render($tabs); ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
        <?php print render($page['content']); ?>
        <?php print $feed_icons; ?>
      </div>

      <?php if ($secondary_menu): ?>
        <nav class="secondary-nav" aria-label="Secondary">
          <?php print theme('links__system_secondary_menu', array(
            'links' => $secondary_menu,
            'attributes' => array(
              'class' => array('links', 'inline', 'clearfix'),
            ),
            'heading' => array(
              'text' => $secondary_menu_heading,
              'level' => 'h2',
              'class' => array('element-invisible'),
            ),
          )); ?>
        </nav>
      <?php endif; ?>

      <?php
      $sidebar_first  = render($page['sidebar_first']);
      $sidebar_second = render($page['sidebar_second']);
      ?>

      <?php if ($sidebar_first || $sidebar_second): ?>
        <aside class="content-sidebar" aria-label="Sidebar">
          <?php print $sidebar_first; ?>
          <?php print $sidebar_second; ?>
        </aside>
      <?php endif; ?>

    </div>
  </main>

</div> <!-- end div.page -->

<footer role="contentinfo">
  <div class="footer-wrapper">

    <div class="footer-center">
      <p class="footer-header">WESTERN WASHINGTON UNIVERSITY<br><?php print $site_name; ?></p>

      <p><?php print $address; ?> &nbsp;·&nbsp; <?php print $phone; ?> &nbsp;·&nbsp; <a href="mailto:<?php print $email; ?>"><?php print $email; ?></a></p>

      <div class="western-social-media">
        <ul>
          <li><a href="https://www.facebook.com/WWUAlumni"><img src="<?php  print $base_path; print $directory ?>/images/icon-facebook.svg" alt="WWU Alumni Facebook"></a></li>
          <li><a href="https://www.flickr.com/photos/wwualumni/"><img src="<?php  print $base_path; print $directory ?>/images/icon-flickr.svg" alt="WWU Alumni Flickr"></a></li>
          <li><a href="https://twitter.com/WWUAlumni"><img src="<?php  print $base_path; print $directory ?>/images/icon-twitter.svg" alt="WWU Alumni Twitter"></a></li>
          <li><a href="https://www.youtube.com/user/WWUalumni"><img src="<?php  print $base_path; print $directory ?>/images/icon-youtube.svg" alt="WWU Alumni YouTube"></a></li>
          <li><a href="https://vimeo.com/wwufoundation"><img src="<?php  print $base_path; print $directory ?>/images/icon-vimeo.svg" alt="WWU Foundation Vimeo"></a></li>
        </ul>
      </div>

      <p class="tagline">Make Waves.</p>
    </div>

    <div class="footer-bottom-bar">
      <p>© Copyright. All Rights Reserved, Western Washington University | WWU is an equal opportunity institution | <a href="https://www.wwu.edu/privacy/">Website Privacy Statement</a> | <a href="https://wwu.edu/commitment-accessibility">Accessibility</a></p>
    </div>

  </div> <!-- end div.footer-wrapper -->
</footer>

<?php print render($page['bottom']); ?>
