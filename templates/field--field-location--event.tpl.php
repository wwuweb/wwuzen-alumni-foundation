<?php

/**
 * @file
 * Template override for field-location on event content type.
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="field-items"<?php print $content_attributes; ?>>
    <?php if (count($items) > 1): ?>
    <div class="event-location-various">Various</div>
    <?php else: ?>
    <?php foreach ($items as $delta => $item): ?>
      <div class="field-item <?php print $delta % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$delta]; ?>>
        <?php hide($item['street_block']); ?>
        <?php hide($item['locality_block']['postal_code']); ?>
        <?php hide($item['country']); ?>
        <?php print render($item); ?>
      </div>
    <?php endforeach; ?>
    <?php endif; ?>
  </div>
</div>
