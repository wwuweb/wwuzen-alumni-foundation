<?php
/**
 * @file
 * Node Quotation template.
 */
?>
<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php print render($content); ?>
</article>
