<?php

/**
 * @file
 * Returns the HTML for an event node rendered with the teaser view mode.
 */
?>
<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <a href="<?php print url('node/' . $node->nid, array('absolute' => TRUE)); ?>">
    <?php print render($content['field_image']); ?>
    <div class="event-bottom-bar">
      <h2 class="event-title"><?php print $title; ?></h2>
      <div class="event-location-date">
        <div class="event-location"><?php print render($content['field_location']); ?></div>
        <div class="event-date"><?php print render($content['field_date']); ?></div>
      </div>
    </div>
  </a>
</article>
