<?php

/**
 * @file
 * Template file override for field-header.
 */
foreach ($items as $delta => $item) {
  print render($item);
}
