<?php
/**
 * @file
 * Customize the Apearance settings page for this theme.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function wwuzen_alumni_foundation_form_system_theme_settings_alter(&$form, &$form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }

  $form['contact'] = array(
    '#type' => 'fieldset',
    '#title' => t('Contact Info'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['contact']['address'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Address'),
    '#default_value' => theme_get_setting('address'),
    '#required'      => TRUE,
  );

  $form['contact']['phone'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Phone'),
    '#default_value' => theme_get_setting('phone'),
    '#required'      => TRUE,
  );

  $form['contact']['email'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Email'),
    '#default_value' => theme_get_setting('email'),
    '#required'      => TRUE,
  );
}
