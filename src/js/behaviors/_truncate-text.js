(function ($, Drupal, window, document, undefined) {

  'use strict';

  Drupal.behaviors.truncateTextOnOverflow = {

    attach: function(context) {
      var $views_rows = $('.view-news .views-row', context);
      var $field_content = $views_rows.find('.views-field-body .field-content');

      $field_content.dotdotdot({
        watch: true
      });
    }
  };

})(jQuery, Drupal, this, this.document);
