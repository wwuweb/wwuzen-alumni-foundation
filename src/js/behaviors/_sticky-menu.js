(function ($, enquire, Drupal, window, document, undefined) {
  $(document).ready(function(){
    var windowWidth = $(window).width();

    if (windowWidth > 800) {
      var scroll = $(window).scrollTop();

      $(window).scroll(function() {
        var lastScroll = scroll;
        scroll = $(window).scrollTop();
        windowWidth = $(window).width();

        /*------------------------------------------------------------------------
          Scrolling Down
        ------------------------------------------------------------------------*/
        if (scroll > lastScroll) {
          // near top of screen - menu should be static
          if (scroll < 150 || windowWidth < 801) {
            $('.header-wrap').removeClass("sticky");
          }

          // past top & menu has been slid down - menu should auto hide
          else {
            $('.header-wrap').slideUp(300, "linear");
          }
        }

        /*------------------------------------------------------------------------
          Scrolling Up
        ------------------------------------------------------------------------*/
        else {
          //at top menu should go static again
          if (scroll < 1) {
            $('.header-wrap').removeClass("sticky");
          }

          if (scroll > 150 && windowWidth > 800) {
            // anywhere else on the page menu should scroll down
            $('.header-wrap').slideDown(300, "linear");
            $('.header-wrap').addClass("sticky");
          }
        }

      });
    }
  });
}(jQuery, enquire, Drupal, this, this.document));
