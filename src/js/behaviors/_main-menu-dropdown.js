(function ($, enquire, Drupal, window, document, undefined) {

  Drupal.behaviors.mainMenuDropdown = {

    attach: function (context) {
      var $main_menu = $('#main-menu .menu-name-main-menu > .menu', context);

      $main_menu.wwuDropdown();
      $main_menu.wwuDropdown('disable');

      enquire.register('all and (min-width: 801px)', {

        match : function () {
          $main_menu.wwuDropdown('enable');
        },

        unmatch : function () {
          $main_menu.wwuDropdown('disable');
        }

      });
    }

  };

})(jQuery, enquire, Drupal, this, this.document);
