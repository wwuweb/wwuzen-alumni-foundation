(function ($, enquire, Drupal, window, document, undefined) {

  Drupal.behaviors.frontPageAnimation = {

    attach: function () {

      var $animation_elements = $('.view-stories .views-field-nothing, ' +
        '.view-stories .views-field-field-image, ' +
        '.node-quotation .field-item, ' +
        '.view-news .views-row, ' +
        '.view-alumni-stars .views-row, ' +
        '.view-alumni-stars .view-header, ' +
        '.view-featured-alumni-star .views-field-nothing, ' +
        '.view-foundation-achievers .views-row, ' +
        '.view-foundation-achievers .view-header, ' +
        '.view-featured-foundation-achiever .views-field-nothing'
      );
      var $window = $(window);

      $.each($animation_elements, function(){
        var $element = $(this);
        $element.addClass('not-in-view');
      });

      $window.on('scroll resize', check_if_in_view);
      $window.trigger('scroll');

      function check_if_in_view() {
        var window_height = $window.height();
        var window_top_position = $window.scrollTop();
        var window_bottom_position = (window_top_position + window_height);

        $.each($animation_elements, function(){
          var $element = $(this);
          var element_height = $element.outerHeight();
          var element_top_position = $element.offset().top;
          var element_bottom_position = (element_top_position + element_height);

          if ((element_bottom_position >= window_top_position) &&
              (element_top_position <= window_bottom_position)) {
            $element.removeClass('not-in-view');
            $element.addClass('in-view');
          }

          if(!$('.not-in-view').length) {
            $window.off('scroll resize', check_if_in_view);
          }

        });


      }
    }

  };

})(jQuery, enquire, Drupal, this, this.document);
